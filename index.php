<?php

// afmeting in browser is ( 700 x 560 )
// afbeelding moet in 2x resolutie ( 1400 x 1120 ) ingevoerd worden in script

//resizeimage(
//    $face_1['afbeelding']['url'],
//    'current',
//    false,
//    false,
//    '400',
//    '800',
//    false
//);

// breakpoints 576 small / 768 medium / 992 large / 1200 extra large

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

resizeimage('test.jpg', 'images/', array(2000,2000), array(768), array(373,50), array(460), false );

function resizeimagelog($msg) {
    echo $msg;
    echo '<br/>';
}

// dummy functions

function get_template_directory() {
    return false;
}

function get_template_directory_uri() {
    return false;
}

function get_site_url() {
    return false;
}

function resizeimage($filename, $folder = 'images/', $small = false, $medium = false , $large = false, $extra_large = false, $lazyload = false, $class = false, $regenerate = false, $log = false)
{

    // settings
    //

    if(!$filename) {
        echo  'img doesnt exist';
        return false;
    }

    if($folder == 'current') {
        $parse_url =  parse_url($filename);
        $path_info = pathinfo($parse_url['path']);
        //$dirname = $path_info['dirname'];
        //$dirname = str_replace('/nieuw', '', $path_info['dirname']);
        //$uploads = wp_upload_dir();
        //$basedir = $uploads['basedir'];
        //$write_folder =  '/Users/mennoevertzen/Sites/talentit/talent-it-wordpress' . $dirname .'/';
        $write_folder =  getcwd() . $dirname .'/';
        $filename = $path_info['basename'];
        $source_file = $filename;
        $source_folder = $write_folder;

    } else {
        $folder = 'images/'; // without prefix slash
        //$folder = get_template_directory() . '/assets/images/';
        $source_file = $filename;
        $source_folder = $folder;
    }

    $quality = 80;

    // check widths

    if(isset($small[0])) {
        $small_width = $small[0];
    } else {
        $small_width = false;
    }

    if(isset($medium[0])) {
        $medium_width = $medium[0];
    } else {
        $medium_width = false;
    }

    if(isset($large[0])) {
        $large_width = $large[0];
    } else {
        $large_width = false;
    }

    if(isset($extra_large[0])) {
        $extra_large_width = $extra_large[0];
    } else {
        $extra_large_width = false;
    }

    // check heights

    if(isset($small[1])) {
        $small_height = $small[1];
    } else {
        $small_height = false;
    }

    if(isset($medium[1])) {
        $medium_height = $medium[1];
    } else {
        $medium_height = false;
    }

    if(isset($large[1])) {
        $large_height = $large[1];
    } else {
        $large_height = false;
    }

    if(isset($extra_large[1])) {
        $extra_large_height = $extra_large[1];
    } else {
        $extra_large_height = false;
    }

    $dest_sizes =
        array(
            array(
                'label' => 'small',
                'width' => $small_width,
                'height' => $small_height,
            ),
            array(
                'label' => 'extra_large',
                'width' => $extra_large_width,
                'height' => $extra_large_height
            ),
            array(
                'label' => 'large',
                'width' => $large_width,
                'height' => $large_height
            ),
            array(
                'label' => 'medium',
                'width' => $medium_width,
                'height' => $medium_height
            )
        );


    //$dest_height = false;

    // scale up for retina display, enter 1, 2 or 3
    $scale_up = 2;

    //
    //
    // end settings

    if ($scale_up > 3) {
        $scale_up = 3;
    }

    // get source image info
    $url = $source_folder . $source_file;

    $image = getimagesize($url);
    $source_width = $image[0];
    $source_height = $image[1];

    $jpeglines = array();
    $output_lines = array();

    foreach ($dest_sizes as $dest_size) {

        resizeimagelog('==new size');

        $current_size_label = $dest_size['label'];
        $dest_width         = $dest_size['width'];
        $dest_height        = $dest_size['height'];

        if($dest_width == '') {
            continue;
        }

        // calculate height based on width change if height is false
        if (!$dest_height) {
            $width_change = $source_width / $dest_width;
            $dest_height = $source_height / $width_change;
        }


        $dest_height = round($dest_height);

        $original_dest_width = $dest_width;
        $original_dest_height = $dest_height;

        // if scale up, run more times
        for ($i = 1; $i <= $scale_up; $i++) {

            //resizeimagelog('====new scale up');

            $dest_width = $original_dest_width;
            $dest_height = $original_dest_height;

            //resizeimagelog('dest width: ' . $dest_width . '');

            if ($i > 1) {
                $suffix = '@' . $i . 'x';
            } else {
                $suffix = false;
            }

            // todo check if this works
            if ($image['mime'] == 'image/jpeg') {
                $extension = 'jpeg';
            } else if ($image['mime'] == 'image/png') {
                $extension = 'png';
            }

            $path_parts = pathinfo($url);
            $dst_dir = $source_folder . $path_parts['filename'] . '-' . $dest_width . 'x' . $dest_height . $suffix; // the new filename

            // generate urls for picture element
            //$output_lines[$i] = $dst_dir;

            // todo: dit moet beter
            if($folder == 'current') {

                //$output_lines[$i] = $source_folder . $path_parts['filename'] . '-' . $dest_width . 'x' . $dest_height . $suffix; // the new filename
                $output_lines[$i] = get_site_url() . $dirname . '/' . $path_parts['filename'] . '-' . $dest_width . 'x' . $dest_height . $suffix; // the new filename

            } else {

                $output_lines[$i] = get_template_directory_uri() . '/' . $folder . $path_parts['filename'] . '-' . $dest_width . 'x' . $dest_height . $suffix; // the new filename
            }

            // check if file already exists
            if(file_exists($dst_dir.'.'.$extension) && $regenerate == false) {
                resizeimagelog($dst_dir.'.'.$extension . ' exists. Skip!');

                // todo: IS DIT NOG NODIG? reset dest height to original, for now false
                //$dest_height = false;

                continue;
            } else {
                resizeimagelog('create: ' . $dst_dir.'.'.$extension . '');
            }

            // check if source image is jpg or png
            if ($image['mime'] == 'image/jpeg') {
                $src_img = imagecreatefromjpeg($url);
            } else if ($image['mime'] == 'image/png') {
                $src_img = imagecreatefrompng($url);
            } else {
                resizeimagelog('extension not allowed');
                die();
            }

            if ($i > 1) {
                $dest_width = $dest_width * $i;
                $dest_height = $dest_height * 2;
            }

            // create new image object
            $dst_img = imagecreatetruecolor($dest_width, $dest_height);

            // preserve alpha channel if png
            if ($image['mime'] == 'image/png') {
                imagecolortransparent($dst_img, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
                imagealphablending($dst_img, false);
                imagesavealpha($dst_img, true);
            }

            $source_width_new = $source_height * $dest_width / $dest_height;
            $source_height_new = $source_width * $dest_height / $dest_width;

            if ($source_width_new > $source_width) {
                //cut point by height
                $h_point = (($source_height - $source_height_new) / 2);
                //copy image
                imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $dest_width, $dest_height, $source_width, $source_height_new);
            } else {
                //cut point by width
                $w_point = (($source_width - $source_width_new) / 2);
                imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $dest_width, $dest_height, $source_width_new, $source_height);
            }

            // now safe the new images

            if ($image['mime'] == 'image/jpeg') {
                imagejpeg($dst_img, $dst_dir.'.'.$extension, $quality);

            } else if ($image['mime'] == 'image/png') {
                imagepng($dst_img, $dst_dir.'.'.$extension, 9);
            }

            if(function_exists('imagewebp')) {
                imagewebp($dst_img, $dst_dir . '.webp', $quality);
            }

            // reset
            $dest_height = false;
        }

        // check if lazyload
        if($lazyload) {
            $src = 'data-src';
            $srcset = 'data-srcset';
            $lazyclass = 'lazyload';
        } else {
            $src = 'src';
            $srcset = 'srcset';
            $lazyclass = false;
        }

        if($lazyload && $class) {
            $class = $lazyclass . ' ' . $class;
        } else {
            $class = $lazyclass;
        }



        switch($current_size_label) {
            case 'small':
                if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false  && function_exists('imagewebp')) {
                    $fallbackurl = $output_lines[1] . '.webp';
                } else {
                    $fallbackurl = $output_lines[1] . '.' . $extension;
                }
                $jpeglines[] = '<source media="(max-width: 768px)" '.$srcset.'="'.$output_lines[1].'.'.$extension.' 1x,'.$output_lines[2].'.'.$extension.' 2x" type="image/'.$extension.'"/>';
                $webplines[] = '<source media="(max-width: 768px)" '.$srcset.'="'.$output_lines[1].'.webp 1x,'.$output_lines[2].'.webp 2x" type="image/webp"/>';
                break;
            case 'medium':
                if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false  && function_exists('imagewebp')) {
                    $fallbackurl = $output_lines[1] . '.webp';
                } else {
                    $fallbackurl = $output_lines[1] . '.' . $extension;
                }
                $jpeglines[] = '<source media="(min-width: 768px)" '.$srcset.'="'.$output_lines[1].'.'.$extension.' 1x,'.$output_lines[2].'.'.$extension.' 2x" type="image/'.$extension.'"/>';
                $webplines[] = '<source media="(min-width: 768px)" '.$srcset.'="'.$output_lines[1].'.webp 1x,'.$output_lines[2].'.webp 2x" type="image/webp"/>';
                break;
            case 'large':
                if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false  && function_exists('imagewebp')) {
                    $fallbackurl = $output_lines[1] . '.webp';
                } else {
                    $fallbackurl = $output_lines[1] . '.' . $extension;
                }
                $jpeglines[] = '<source media="(min-width: 992px)" '.$srcset.'="'.$output_lines[1].'.'.$extension.' 1x,'.$output_lines[2].'.'.$extension.' 2x" type="image/'.$extension.'"/>';
                $webplines[] = '<source media="(min-width: 992px)" '.$srcset.'="'.$output_lines[1].'.webp 1x,'.$output_lines[2].'.webp 2x" type="image/webp"/>';
                break;
            case 'extra_large':
                if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false  && function_exists('imagewebp')) {
                    $fallbackurl = $output_lines[1] . '.webp';
                } else {
                    $fallbackurl = $output_lines[1] . '.' . $extension;
                }
                $jpeglines[] = '<source media="(min-width: 1200px)" '.$srcset.'="'.$output_lines[1].'.'.$extension.' 1x,'.$output_lines[2].'.'.$extension.' 2x" type="image/'.$extension.'"/>';
                $webplines[] = '<source media="(min-width: 1200px)" '.$srcset.'="'.$output_lines[1].'.webp 1x,'.$output_lines[2].'.webp 2x" type="image/webp"/>';
                break;
        }
    }


    // output the picture tag with jpg, png or webp

    ob_start();
    ?>

    <picture>
        <?php
        if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false && function_exists('imagewebp') ) {
            foreach($webplines as $webpline) {
                echo $webpline;
            }
        } else {
            foreach ($jpeglines as $jpegline) {
                echo $jpegline;
            }
        }
        ?>
        <img class="<?php echo $class ?>" <?php echo $src ?>="<?php echo $fallbackurl ?>"/>
    </picture>

    <?php
    $content = ob_get_contents();
    ob_end_clean();
    echo $content;
}
